package com.game.wanderer.window;

import com.game.wanderer.view.Board;

import javax.swing.*;
import java.awt.*;

public class WindowCreator extends JFrame {

    public void setWindow() {
        setTitle("Warender");
        Board board = new Board();
        add(board);
        setBackground(Color.BLACK);
        setFocusable(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        pack();
        setLocationRelativeTo(null);
        addKeyListener(board);
        setResizable(false);
    }
}
