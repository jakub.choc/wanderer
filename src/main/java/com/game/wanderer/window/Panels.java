package com.game.wanderer.window;

import javax.swing.*;
import java.awt.*;

public class Panels {

    private Graphics graphics;
    private JPanel jPanel;
    private JButton playAgain;

    public Panels(Graphics graphics, JPanel jPanel) {
        this.graphics = graphics;
        this.jPanel = jPanel;
    }

    public void generateDeadPanel() {
        jPanel.add(playAgain);
        graphics.setFont(new Font("Arial", Font.BOLD, 40));
        graphics.setColor(Color.BLACK);
        graphics.drawString("YOU DIED", 340, 300);
        jPanel.paint(graphics);
    }
}
