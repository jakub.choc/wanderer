package com.game.wanderer.window;

import com.game.wanderer.characters.Diamond;
import com.game.wanderer.characters.Hero;
import com.game.wanderer.characters.Portal;
import com.game.wanderer.characters.Skeleton;
import com.game.wanderer.characters.Vendor;
import com.game.wanderer.map.Map;
import com.game.wanderer.util.*;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
@Setter
public class Game {

    private LevelRules levelRules;
    private Map map;
    private Hero hero;
    private List<Skeleton> skeletons;
    private List<Diamond> diamonds;
    private Portal portal;
    private boolean startGame;
    private SoundPlayer soundPlayer;
    private Save save;
    private Vendor vendor;

    public Game() {
        this.levelRules = LevelRules.LEVEL_1;
        this.map = new Map(levelRules.map);
        this.diamonds = new ArrayList<>();
        this.save = new Save();
        this.vendor = new Vendor();
        this.soundPlayer = new SoundPlayer();
    }

    public void createMap() {
        this.map = new Map(levelRules.map);
    }

    public void createPortal() {
        this.portal = new Portal(getMap().getMapSchema());
    }

    public void createHero() {
        this.hero = new Hero(getMap(), getSkeletons(), getDiamonds(), getLevelRules());
    }

    public void createSkeletons() {
        List<Skeleton> newSkeletons = new ArrayList<>();
        for (int i = 0; i < levelRules.skeletonAmount; i++) {
            Skeleton skeleton = new Skeleton(getMap(), getLevelRules());
            this.skeletons = new ArrayList<>();
            newSkeletons.add(skeleton);
        }
        setSkeletons(newSkeletons);
    }

    public void nextLevel() {
        if (hero.getX() == portal.getX() && hero.getY() == portal.getY()) {
            int nextLevel = getLevelRules().level + 1;
            LevelRules rules = Arrays.stream(LevelRules.values()).filter(l -> l.level == nextLevel).findFirst().orElse(null);
            if (rules == null) {
                setLevelRules(LevelRules.LEVEL_1);
                save.overrideLevel(1);
            } else {
                setLevelRules(rules);
                save.overrideLevel(nextLevel);
            }
            setMap(null);
            setHero(null);
            setSkeletons(null);
            setDiamonds(new ArrayList<>());
            setPortal(null);
            soundPlayer.playSound(GameConstants.NEXT_LEVEL);
        }
    }

    public void repeatLevel() {
        setMap(null);
        setHero(null);
        setSkeletons(null);
        setDiamonds(new ArrayList<>());
        setPortal(null);
    }

    public void newGame() {
        setLevelRules(LevelRules.LEVEL_1);
        save.overrideLevel(1);
    }

    public void continueGame() {
        int level = save.readSavedLevel();
        if (level > LevelRules.values().length) {
            newGame();
        }
        if (level != -1) {
            LevelRules rules = Arrays.stream(LevelRules.values()).filter(l -> l.level == level).findFirst().orElse(null);
            setLevelRules(rules);
        }
    }

}
