package com.game.wanderer.map;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Map {

    private int [][] mapSchema = new int[10][10];

    public Map(String mapFile) {
        readMap(mapFile);
    }

    public int [][] getMapSchema() {
        return mapSchema;
    }

    public void readMap(String mapFile) {
        try {
            FileReader fileReader = new FileReader(mapFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            int lineNumber = 0;
            while ((line = bufferedReader.readLine()) != null) {
                addLine(line, lineNumber);
                lineNumber ++;
            }

            bufferedReader.close();
            fileReader.close();
        } catch (
                IOException e) {
            throw new NullPointerException();
        }
    }

    private void addLine(String line, int lineNumber) {
        char[] charArray = line.toCharArray();
        for (int i = 0; i < line.length(); i++) {
            char index = charArray[i];
            int numberValue = Character.getNumericValue(index);
            mapSchema[lineNumber][i] = numberValue;
        }
    }

    public boolean isFloor(int x, int y) {
        int [][] schema = getMapSchema();
        int xDirection = 0;
        int yDirection = 0;
        if (x != 0) {
            xDirection = x / 72;
        }

        if (y != 0) {
            yDirection = y / 72;
        }

        if (schema[yDirection][xDirection] == 1) {
            return true;
        }
        return false;
    }
}
