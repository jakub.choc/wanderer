package com.game.wanderer.characters;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class Vendor {

    int x;
    int y;
    List<Item> inventory;
    private JDialog vendorDialog;

    public Vendor() {

    }

    public void showVendorDialog(Component c) {
        JButton jButton = new JButton("Click");
        jButton.setSize(new Dimension(100, 100));
        vendorDialog = new JDialog(vendorDialog, "Vendor", true);
        vendorDialog.setLayout(new FlowLayout());
        JLabel label = new JLabel("Vendor");
        vendorDialog.add(label);
        vendorDialog.add(jButton);
        vendorDialog.setSize(600, 300);
        vendorDialog.setLocationRelativeTo(c);
        vendorDialog.setVisible(true);
        vendorDialog.setResizable(false);
    }
}
