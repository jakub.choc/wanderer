package com.game.wanderer.characters;

import java.util.List;
import com.game.wanderer.map.Map;
import com.game.wanderer.util.GameConstants;
import com.game.wanderer.util.LevelRules;
import com.game.wanderer.util.SoundPlayer;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Hero extends Character {

    private List<Skeleton> skeletons;
    private List<Diamond> diamonds;
    private List<Item> inventory;
    private LevelRules levelRules;
    private SoundPlayer soundPlayer;

    public Hero(Map map, List<Skeleton> skeletons, List<Diamond> diamonds, LevelRules levelRules) {
        super(map, GameConstants.HERO_DOWN);
        this.levelRules = levelRules;
        this.skeletons = skeletons;
        this.diamonds = diamonds;
        setHP();
        setDefendPoints();
        setStrikePoints();
        setDiamondsAmount(0);
        soundPlayer = new SoundPlayer();
    }

    public void right() {
        if (getX() == 648 || !getMap().isFloor(getX() + 72, getY())) {
            setIcon(GameConstants.HERO_RIGHT);
        } else {
            setX(getX() + 72);
            setIcon(GameConstants.HERO_RIGHT);
        }
    }

    public void left() {
        if (getX() == 0 || !getMap().isFloor(getX() - 72, getY())) {
            setIcon(GameConstants.HERO_LEFT);
        } else {
            setX(getX() - 72);
            setIcon(GameConstants.HERO_LEFT);
        }
    }

    public void down() {
        if (getY() == 648 || !getMap().isFloor(getX(), getY() + 72)) {
            setIcon(GameConstants.HERO_DOWN);
        } else {
            setY(getY() + 72);
            setIcon(GameConstants.HERO_DOWN);
        }
    }

    public void up() {
        if (getY() == 0 || !getMap().isFloor(getX(), getY() - 72)) {
            setIcon(GameConstants.HERO_UP);
        } else {
            setY(getY() - 72);
            setIcon(GameConstants.HERO_UP);
        }
    }

    public void attack() {
        Skeleton enemy = findNearEnemy();
        if (enemy != null) {
            attack(enemy);
            if (enemy.isAlive()) {
                enemy.attack(this);
            }
            soundPlayer.playSound(GameConstants.SMALL_HIT);
            if (!enemy.isAlive()) {
                diamonds.add(new Diamond(enemy.getX(), enemy.getY()));
                skeletons.remove(enemy);
                setHpAfterFight();
            }
            if (!isAlive()) {
                setDead(true);
            }
        }
    }

    public Skeleton findNearEnemy() {
        for (Skeleton skeleton : skeletons) {
            if (skeleton.getX() == getX() + 72 && skeleton.getY() == getY()) {
                return skeleton;
            }
            if (skeleton.getX() == getX() - 72 && skeleton.getY() == getY()) {
                return skeleton;
            }
            if (skeleton.getX() == getX() && skeleton.getY() -72 == getY()) {
                return skeleton;
            }
            if (skeleton.getX() == getX() && skeleton.getY() +72 == getY()) {
                return skeleton;
            }
        }
        return null;
    }

    public void takeItem() {
        for (Diamond diamond : getDiamonds()) {
            if (diamond.getX() == getX() && diamond.getY() == getY()) {
                diamonds.remove(diamond);
                setDiamondsAmount(getDiamondsAmount() + 1);
                soundPlayer.playSound(GameConstants.COIN);
                break;
            }
        }
    }

    private void setHP() {
        int hp = 20 + levelRules.level;
        setMaxHP(hp);
        setHealthPoints(hp);
    }

    private void setDefendPoints() {
        int dp = 2 + levelRules.level;
        setArmour(dp);
    }

    private void setStrikePoints() {
        int sp = 5 + levelRules.level;
        setDamage(sp);
    }

    private void setHpAfterFight() {
        int newHp = getRandomNumber();
        if (getHealthPoints() + newHp <= getMaxHP()) {
            setHealthPoints(getHealthPoints() + newHp);
        } else {
            int dif = (getHealthPoints() + newHp) - getMaxHP();
            int result = newHp - dif;
            setHealthPoints(getHealthPoints() + result);
        }
    }


}
