package com.game.wanderer.characters;

import java.util.Random;
import com.game.wanderer.map.Map;
import com.game.wanderer.util.GameConstants;
import com.game.wanderer.util.LevelRules;

public class Skeleton extends Character {

    private final LevelRules levelRules;

    public Skeleton(Map map, LevelRules levelRules) {
        super(map, GameConstants.SKELETON);
        this.levelRules = levelRules;
        setRandomPosition();
        setHP();
        setDefendPoints();
        setStrikePoints();
    }

    private void setRandomPosition() {
        Random random;
        random = new Random();
        int [][] mapStructure = getMap().getMapSchema();
        boolean isFree = false;
        int xDirection;
        int yDirection;
        do {
            xDirection = random.nextInt(mapStructure.length);
            yDirection = random.nextInt(mapStructure.length);
            if (mapStructure[yDirection][xDirection] != 0 || yDirection == 0 && xDirection == 0) {
                isFree = true;
            }

        } while (isFree != true);
        setX(xDirection * 72);
        setY(yDirection * 72);
    }

    private void setHP() {
        int hp = (4 + levelRules.getLevel() + getRandomNumber());
        setMaxHP(hp);
        setHealthPoints(hp);
    }

    private void setDefendPoints() {
        int dp = 5 + levelRules.level;
        setArmour(dp);
    }

    private void setStrikePoints() {
        int sp = 5 + levelRules.level;
        setDamage(sp);
    }

}
