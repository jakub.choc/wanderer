package com.game.wanderer.characters;

import com.game.wanderer.util.GameConstants;

public class Diamond extends Item {

    public Diamond(int x, int y) {
        super(GameConstants.DIA, x, y);
    }
}
