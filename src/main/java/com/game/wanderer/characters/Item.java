package com.game.wanderer.characters;

import com.game.wanderer.view.PositionedImage;
import lombok.Getter;
import lombok.Setter;

import java.awt.*;
import java.util.Random;

@Getter
@Setter
public class Item {

    private int x;
    private int y;
    private String icon;
    private int [][] map;

    public Item(String icon, int [][] map) {
        this.icon = icon;
        this.map = map;
        setRandomPosition(map);
    }

    public Item(String icon, int x, int y) {
        this.icon = icon;
        this.x = x;
        this.y = y;
    }

    public void drawImage(Graphics2D graphics2D) {
        PositionedImage image = new PositionedImage(getIcon(), getX(), getY());
        image.draw(graphics2D);
    }

    private void setRandomPosition(int [][] map) {
        Random random;
        random = new Random();
        boolean isFree = false;
        int xDirection;
        int yDirection;
        do {
            xDirection = random.nextInt(map.length);
            yDirection = random.nextInt(map.length);
            if (map[yDirection][xDirection] != 0 || yDirection == 0 && xDirection == 0) {
                isFree = true;
            }

        } while (isFree != true);
        setX(xDirection * 72);
        setY(yDirection * 72);
    }
}
