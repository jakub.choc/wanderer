package com.game.wanderer.characters;

import com.game.wanderer.util.GameConstants;


public class Portal extends Item {

    public Portal(int [][] map) {
        super(GameConstants.PORTAL, map);
    }

}
