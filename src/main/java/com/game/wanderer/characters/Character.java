package com.game.wanderer.characters;

import com.game.wanderer.map.Map;
import com.game.wanderer.view.PositionedImage;
import lombok.Getter;
import lombok.Setter;

import java.awt.*;
import java.util.Random;

@Getter
@Setter
public class Character {
    private int healthPoints;
    private int maxHP;
    private int armour;
    private int damage;
    private int x;
    private int y;
    private boolean dead;
    private int level;
    private Map map;
    private int diamondsAmount;
    private String icon;

    public Character(Map map, String icon) {
        this.level = 1;
        this.x = 0;
        this.y = 0;
        dead = false;
        this.map = map;
        this.icon = icon;
        this.diamondsAmount = 0;
    }

    public boolean isAlive() {
        return healthPoints > 0;
    }

    public void drawImage(Graphics2D graphics2D) {
        PositionedImage image = new PositionedImage(getIcon(), getX(), getY());
        image.draw(graphics2D);
    }

    public void attack(Character character) {
        double damage = getDamage() + getRandomNumber();
        double finalDamage = damage - (character.getArmour() * 0.5);
        character.setHealthPoints(character.getHealthPoints() - (int)finalDamage);
    }

    public int getRandomNumber() {
        Random random = new Random();
        return random.nextInt(6);
    }
}
