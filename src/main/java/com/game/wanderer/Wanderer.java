package com.game.wanderer;

import com.game.wanderer.window.WindowCreator;

public class Wanderer {

    public static void main(String[] args) {
        WindowCreator windowCreator = new WindowCreator();
        windowCreator.setWindow();
    }
}
