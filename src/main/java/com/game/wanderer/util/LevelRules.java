package com.game.wanderer.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum LevelRules {

    LEVEL_1(1, GameConstants.MAP_1_LEVEL, false, GameConstants.SKELETON, 2),
    LEVEL_2(2, GameConstants.MAP_2_LEVEL, false, GameConstants.SKELETON,  3),
    LEVEL_3(3, GameConstants.MAP_3_LEVEL, false, GameConstants.SKELETON,  3),
    LEVEL_4(4, GameConstants.MAP_4_LEVEL, false, GameConstants.SKELETON,  3),
    LEVEL_5(5, GameConstants.MAP_5_LEVEL, false, GameConstants.SKELETON,  3),
    LEVEL_6(6, GameConstants.MAP_6_LEVEL, false, GameConstants.SKELETON,  3),
    LEVEL_7(7, GameConstants.MAP_7_LEVEL, false, GameConstants.SKELETON,  3),
    LEVEL_8(8, GameConstants.MAP_8_LEVEL, false, GameConstants.SKELETON,  3),
    LEVEL_9(9, GameConstants.MAP_9_LEVEL, false, GameConstants.SKELETON,  3),
    LEVEL_10(10, GameConstants.MAP_10_LEVEL, false, GameConstants.BOSS,  1);

    public final int level;
    public final String map;
    public final boolean hasVendor;
    public final String skeleton_icon;
    public final int skeletonAmount;

}
