package com.game.wanderer.util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class ButtonCreator {

    public JButton createButton(String text, Color bgColor, Color textColor, int x, int y, ActionListener a) {
        JButton button = new JButton(text);
        button.setBackground(bgColor);
        button.setForeground(textColor);
        button.setFocusPainted(false);
        button.setBounds(x, y, 200, 100);
        button.setBorder(BorderFactory.createRaisedBevelBorder());
        button.addActionListener(a);
        button.setText(text);
        return button;
    }
}
