package com.game.wanderer.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Save {

    public int readSavedLevel() {
        try (BufferedReader reader = new BufferedReader(new FileReader(GameConstants.LOAD_GAME))) {
            String line = reader.readLine();
            if (line != null) {
                return Integer.parseInt(line);
            } else {
                System.out.println("The file is empty.");
            }
        } catch (IOException e) {
            throw new RuntimeException("File reading failed");
        } catch (NumberFormatException e) {
            throw new NumberFormatException("The file does not contain a valid number.");
        }
        return -1;
    }

    public void overrideLevel(int newNumber) {
        try (FileWriter writer = new FileWriter(GameConstants.LOAD_GAME)) {
            writer.write(String.valueOf(newNumber));
        } catch (IOException e) {
            System.err.println("Error writing to the file: " + e.getMessage());
        }
    }
}
