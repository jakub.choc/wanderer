package com.game.wanderer.util;

public class GameConstants {

    //PICS
    public static final String HERO_LEFT = "src/main/resources/img/hero-left.png";
    public static final String HERO_RIGHT = "src/main/resources/img/hero-right.png";
    public static final String HERO_UP = "src/main/resources/img/hero-up.png";
    public static final String HERO_DOWN = "src/main/resources/img/hero-down.png";
    public static final String SKELETON = "src/main/resources/img/skeleton.png";
    public static final String WALL = "src/main/resources/img/wall.png";
    public static final String FLOOR = "src/main/resources/img/floor.png";
    public static final String DIA_ICON = "src/main/resources/img/dia-icon.png";
    public static final String DIA = "src/main/resources/img/dia.png";
    public static final String BOSS = "src/main/resources/img/boss.png";
    public static final String PORTAL = "src/main/resources/img/portal.png";
    public static final String VENDOR = "src/main/resources/img/vendor.png";

    //SOUNDS
    public static final String COIN = "src/main/resources/sounds/coin.wav";
    public static final String SMALL_HIT = "src/main/resources/sounds/small-hit.wav";
    public static final String SNEAKY_SNITCH = "src/main/resources/sounds/sneaky-snitch.wav";
    public static final String NEXT_LEVEL = "src/main/resources/sounds/next-level.wav";

    //MAPS
    public static final String MAP_1_LEVEL = "src/main/resources/maps/level_1_map.txt";
    public static final String MAP_2_LEVEL = "src/main/resources/maps/level_2_map.txt";
    public static final String MAP_3_LEVEL = "src/main/resources/maps/level_3_map.txt";
    public static final String MAP_4_LEVEL = "src/main/resources/maps/level_4_map.txt";
    public static final String MAP_5_LEVEL = "src/main/resources/maps/level_5_map.txt";
    public static final String MAP_6_LEVEL = "src/main/resources/maps/level_6_map.txt";
    public static final String MAP_7_LEVEL = "src/main/resources/maps/level_7_map.txt";
    public static final String MAP_8_LEVEL = "src/main/resources/maps/level_8_map.txt";
    public static final String MAP_9_LEVEL = "src/main/resources/maps/level_9_map.txt";
    public static final String MAP_10_LEVEL = "src/main/resources/maps/level_10_map.txt";

    //LEVEL
    public static final String LOAD_GAME = "src/main/resources/saveGame/lastRound.txt";

}
