package com.game.wanderer.view;

import com.game.wanderer.characters.Diamond;
import com.game.wanderer.characters.Hero;
import com.game.wanderer.characters.Skeleton;
import com.game.wanderer.util.ButtonCreator;
import com.game.wanderer.util.GameConstants;
import com.game.wanderer.window.Game;
import lombok.Getter;
import lombok.Setter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

@Getter
@Setter
public class Board extends JComponent implements KeyListener, ActionListener {

    private Game game;
    private JButton startButton;
    private JButton continueButton;
    private JButton exitButton;
    private JButton playAgain;
    private JButton endGame;
    private final ButtonCreator buttonCreator;
    private boolean startGame;
    private static final String FONT = "Arial";

    public Board() {
        setPreferredSize(new Dimension(900, 720));
        addKeyListener(this);
        game = new Game();
        this.buttonCreator = new ButtonCreator();
        generateStartPanel();
    }

    private void addButton() {
        continueButton = buttonCreator.createButton("CONTINUE", Color.LIGHT_GRAY, Color.BLACK, 350, 200, this);
        startButton =  buttonCreator.createButton("NEW GAME", Color.LIGHT_GRAY, Color.BLACK, 350, 300, this);
        exitButton =  buttonCreator.createButton("EXIT GAME", Color.LIGHT_GRAY, Color.BLACK, 350, 400, this);
        playAgain =  buttonCreator.createButton("PLAY AGAIN", Color.LIGHT_GRAY, Color.BLACK, 350, 400, this);
        endGame = buttonCreator.createButton("END GAME", Color.LIGHT_GRAY, Color.BLACK, 350, 400, this);
    }

    @Override
    public void paint(Graphics graphics) {
        super.paint(graphics);
        Graphics2D g2d = (Graphics2D) graphics.create();
        if (startGame) {
            if (game.getHero() != null && game.getHero().isDead()) {
                generateDeadPanel(graphics);
            } else {
                removeAll();
                generateGame(g2d);
            }
        }
    }

    private void generateGame(Graphics2D g2d) {
        generateGameMap(g2d);
        generateSkeletons(g2d);
        generateHero(g2d);
        generateDiamonds(g2d);
        generateStatus(g2d);
        generatePortal(g2d);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (event.getSource().equals(startButton)) {
            setStartGame(true);
            game.newGame();
            repaint();
        }
        if (event.getSource().equals(exitButton)) {
            System.exit(1);
        }
        if (event.getSource().equals(playAgain)) {
            game.repeatLevel();
            repaint();
        }
        if (event.getSource().equals(continueButton)) {
            game.continueGame();
            setStartGame(true);
            repaint();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            game.getHero().up();
        } else if(e.getKeyCode() == KeyEvent.VK_DOWN) {
            game.getHero().down();
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            game.getHero().right();
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            game.getHero().left();
        } else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            if (!game.getDiamonds().isEmpty()) {
                game.getHero().takeItem();
            }
            if (game.getSkeletons().isEmpty()) {
                game.nextLevel();
            } else {
                game.getHero().attack();
            }
        }
        repaint();
    }

    private void generateGameMap(Graphics2D graphics) {
        if (game.getMap() == null) {
            game.createMap();
        }
        PositionedImage image;
        for (int y = 0; y < game.getMap().getMapSchema().length; y++) {
            for (int x = 0; x < game.getMap().getMapSchema().length; x++) {
                if (game.getMap().getMapSchema()[y][x] == 1) {
                    image = new PositionedImage(GameConstants.FLOOR, calcPosition(x), calcPosition(y));
                    image.draw(graphics);
                } else {
                    image = new PositionedImage(GameConstants.WALL,  calcPosition(x), calcPosition(y));
                    image.draw(graphics);
                }
            }
        }
    }

    private void generateHero(Graphics2D graphics) {
        if (game.getHero() == null) {
            game.createHero();
            game.getHero().drawImage(graphics);
        }
        game.getHero().drawImage(graphics);
    }

    private void generateSkeletons(Graphics2D graphics) {
        if (game.getSkeletons() == null) {
            game.createSkeletons();
        }
        for (Skeleton skeleton : game.getSkeletons()) {
            skeleton.drawImage(graphics);
        }
    }

    private void generateStatus(Graphics2D graphics) {
        PositionedImage image;
        //Game
        Hero hero = game.getHero();
        graphics.setFont(new Font(FONT, Font.BOLD, 20));
        graphics.setColor(Color.black);
        graphics.drawString(String.format("LEVEL: %d", game.getLevelRules().level), getWidth() - 160, getHeight() - 680);

        //Hero
        graphics.setFont(new Font(FONT, Font.BOLD, 15));
        graphics.drawString("HERO", getWidth() - 160, getHeight() - 640);
        graphics.setFont(new Font(FONT, Font.BOLD, 12));
        graphics.drawString(String.format("Level: %d", hero.getLevel()),
                getWidth() - 160, getHeight() - 620);
        graphics.drawString(String.format("HP: %d/%d", hero.getHealthPoints(), hero.getMaxHP()),
                getWidth() - 160, getHeight() - 600);
        graphics.drawString(String.format("Armour: %d", hero.getArmour()), getWidth() - 160, getHeight() - 580);
        graphics.drawString(String.format("Damage: %d", hero.getDamage()), getWidth() - 160, getHeight() - 560);
        image = new PositionedImage(GameConstants.DIA_ICON, getWidth() - 160, getHeight() - 540);
        image.draw(graphics);
        graphics.drawString(String.format("%d", hero.getDiamondsAmount()), getWidth() - 110, getHeight() - 515);

        //Skeletons
        for (Skeleton skeleton : game.getSkeletons()) {
            String skeletonsStatus = String.format("HP: %d/%d", skeleton.getHealthPoints(), skeleton.getMaxHP());
            graphics.setFont(new Font(FONT, Font.BOLD, 15));
            graphics.setColor(Color.white);
            graphics.drawString(skeletonsStatus, skeleton.getX() + 8, skeleton.getY() + 10);
        }

        //Skeleton stats fight
        Skeleton skeleton = game.getHero().findNearEnemy();
        if (skeleton != null) {
            graphics.setFont(new Font(FONT, Font.BOLD, 15));
            graphics.setColor(Color.black);
            graphics.drawString("ENEMY", getWidth() - 160, getHeight() - 470);
            graphics.setFont(new Font(FONT, Font.BOLD, 12));
            graphics.drawString(String.format("Level: %d", skeleton.getLevel()), getWidth() - 160, getHeight() - 450);
            graphics.drawString(String.format("HP: %d/%d", skeleton.getHealthPoints(), skeleton.getMaxHP()),
                    getWidth() - 160, getHeight() - 430);
            graphics.drawString(String.format("Armour: %d", skeleton.getArmour()), getWidth() - 160, getHeight() - 410);
            graphics.drawString(String.format("Damage: %d", skeleton.getDamage()), getWidth() - 160, getHeight() - 390);
        }

        //Inventory
        graphics.setFont(new Font(FONT, Font.BOLD, 15));
        graphics.setColor(Color.black);
        graphics.drawString("INVENTORY", getWidth() - 160, getHeight() - 340);
        graphics.setFont(new Font(FONT, Font.BOLD, 12));
        image = new PositionedImage("src/main/resources/img/item.png", getWidth() - 160, getHeight() - 320);
        image.draw(graphics);
        graphics.drawString("head", getWidth() - 100, getHeight() - 294);
        image = new PositionedImage("src/main/resources/img/item.png", getWidth() - 160, getHeight() - 268);
        image.draw(graphics);
        graphics.drawString("body", getWidth() - 100, getHeight() - 242);
        image = new PositionedImage("src/main/resources/img/item.png", getWidth() - 160, getHeight() - 216);
        image.draw(graphics);
        graphics.drawString("legs", getWidth() - 100, getHeight() - 190);
        image = new PositionedImage("src/main/resources/img/item.png", getWidth() - 160, getHeight() - 164);
        image.draw(graphics);
        graphics.drawString("shoes", getWidth() - 100, getHeight() - 138);
        image = new PositionedImage("src/main/resources/img/item.png", getWidth() - 160, getHeight() - 107);
        image.draw(graphics);
        graphics.drawString("weapon", getWidth() - 155, getHeight() - 40);
        image = new PositionedImage("src/main/resources/img/item.png", getWidth() - 100, getHeight() - 107);
        image.draw(graphics);
        graphics.drawString("shield", getWidth() - 90, getHeight() - 40);
    }

    private void generatePortal(Graphics2D graphics2D) {
        if (game.getSkeletons().isEmpty()) {
            if (game.getPortal() == null) {
                game.createPortal();
            }
            game.getPortal().drawImage(graphics2D);
        }
    }

    private void generateDiamonds(Graphics2D graphics) {
        if (!game.getDiamonds().isEmpty()) {
            for (Diamond diamond : game.getDiamonds()) {
                diamond.drawImage(graphics);
            }
        }
    }

    private int calcPosition(int index) {
        if (index == 0) {
            return 0;
        }
        return index * 72;
    }

    private void generateDeadPanel(Graphics graphics) {
        add(playAgain);
        graphics.setFont(new Font(FONT, Font.BOLD, 40));
        graphics.setColor(Color.BLACK);
        graphics.drawString("YOU DIED", 340, 300);
        super.paint(graphics);
    }

    private void generateStartPanel() {
        addButton();
        add(startButton);
        add(exitButton);
        add(continueButton);
    }

    private void generateEnd(Graphics graphics) {
        add(endGame);
        graphics.setFont(new Font(FONT, Font.BOLD, 40));
        graphics.setColor(Color.BLACK);
        graphics.drawString("END GAME", 340, 300);
        super.paint(graphics);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

}
